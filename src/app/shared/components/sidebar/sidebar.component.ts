import { Component } from '@angular/core';

interface menuItem {
  title:string;
  router:string;
}

@Component({
  selector: 'shared-sidebar',
  templateUrl: './sidebar.component.html',
  styles: ``
})
export class SidebarComponent {

public reactiveMenu:menuItem[]=[
  {title:'Básicos',router:'/reactive/basic'},
  {title:'Dinamicos',router:'/reactive/dynamic'},
  {title:'Switches',router:'/reactive/switches'}
]
public authMenu:menuItem[]=[
  {title:'Registro',router:'/auth/sing-out'} 
]
}
